<?php 
$config = parse_ini_file('config.php');
try {
    $dsn = "mysql:host={$config['host']};dbname={$config['dbnombre']}";
    $options = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );
    $dbh = new PDO($dsn, $config['usuario'], $config['clave']);

    // sanitizar nuestros datos 
    $query = $dbh->query("SELECT officeCode,city,state FROM offices");
    $oficinas = $query->fetchAll(PDO::FETCH_OBJ);

    //prepare hace una sanitizacion de datos
    $empId = '1143';
    $query = $dbh->prepare("SELECT employeeNumber, firstName, lastName FROM employees WHERE employeeNumber <= :empid");
    $query->execute([':empid' => $empId]);
    $jefes = $query->fetchAll(PDO::FETCH_OBJ);

    $query = $dbh->query("SELECT DISTINCT country FROM offices ORDER BY 1 ASC");
    $paises = $query->fetchAll(PDO::FETCH_OBJ);


} catch (PDOException $e) {
    echo $e->getMessage();
}

if ($_POST) {
    echo ($_POST['firstName']);
    echo "<pre>";
    echo "</pre>";
}   