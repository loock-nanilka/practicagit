<?php
try {
    $config = parse_ini_file('config.php');
    $city = $_POST['city'];
    $dsn = "mysql:host={$config['host']};dbname={$config['dbnombre']}";
    $options = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );
    $dbh = new PDO($dsn, $config['usuario'], $config['clave']);

    $query = $dbh->prepare("SELECT officeCode,city,state FROM offices WHERE country like :ciudad");
    $query->execute([':ciudad' => $city]);
    $oficinas = $query->fetchAll(PDO::FETCH_OBJ);
    $lista = '<option value="">-- Selecciona una opci&oacute;n --</option>';
    foreach($oficinas as $oficina) {
        $lista .= "<option value='{$oficina->officeCode}'>{$oficina->city}({$oficina->state})</option>";
    }    
    echo $lista;    
} catch (PDOException $e) {
    echo $e->getMessage();
}